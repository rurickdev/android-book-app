package dev.rurick.android.bootcamp.challenge.books.models.book

import dev.rurick.android.bootcamp.challenge.books.models.LinkContainer
import kotlinx.serialization.Serializable

@Serializable
data class BookRelationships(
    val authors: LinkContainer,
    val categories: LinkContainer
)