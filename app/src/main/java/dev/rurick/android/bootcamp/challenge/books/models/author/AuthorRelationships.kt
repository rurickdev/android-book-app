package dev.rurick.android.bootcamp.challenge.books.models.author

import dev.rurick.android.bootcamp.challenge.books.models.LinkContainer
import kotlinx.serialization.Serializable

@Serializable
data class AuthorRelationships(
    val books: LinkContainer,
)