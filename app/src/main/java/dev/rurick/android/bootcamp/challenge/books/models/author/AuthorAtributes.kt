package dev.rurick.android.bootcamp.challenge.books.models.author

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AuthorAttributes(
    @SerialName("created-at")
    val createdAt: String,
    val name: String,
    @SerialName("updated-at")
    val updatedAt: String
)