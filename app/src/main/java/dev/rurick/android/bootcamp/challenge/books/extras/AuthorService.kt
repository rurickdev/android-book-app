package dev.rurick.android.bootcamp.challenge.books.extras

import android.content.Context
import android.util.Log
import android.view.View
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.snackbar.Snackbar
import dev.rurick.android.bootcamp.challenge.books.models.author.Author
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.json.JSONObject


private const val fileTag = "[AUTHOR]"

fun getAuthorFromBook(
    context: Context,
    parentView: View,
    url: String,
    onResponse: (books: Author) -> Unit
) {
    val logTag = "$fileTag - GET_ALL_BOOKS"

    val allBooksRequest = object : JsonObjectRequest(
        Method.GET,
        url,
        null,
        { response ->
            val authorJson = JSONObject(response.toString()).getJSONObject("data").toString()
            val author= Json.decodeFromString<Author>(authorJson)
            onResponse(author)
        },
        { error ->
            Log.e(
                logTag,
                "fetch books failed with status code ${error?.networkResponse?.statusCode}"
            )
            Snackbar.make(parentView, "Wrong Credentials", Snackbar.LENGTH_SHORT).show()
        },
    ) {
        override fun getHeaders(): Map<String, String> {
            val params: MutableMap<String, String> = mutableMapOf()
            params["Authorization"] = "Bearer " + getSessionToken(context)
            params["Accept"] = "application/json"
            params["Content-Type"] = "application/json"
            return params
        }
    }
    makeHTTPRequest(context, parentView, allBooksRequest)
}