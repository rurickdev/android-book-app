package dev.rurick.android.bootcamp.challenge.books

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import dev.rurick.android.bootcamp.challenge.books.extras.getAuthorFromBook
import dev.rurick.android.bootcamp.challenge.books.models.book.Book
import kotlinx.android.synthetic.main.activity_book_deatils.*
import kotlinx.android.synthetic.main.activity_sign_in.appBarLayout
import kotlinx.serialization.json.Json
import kotlinx.serialization.decodeFromString

class BookDetailsActivity : AppCompatActivity() {
    private lateinit var book : Book
    private lateinit var parentView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_deatils)

        parentView = findViewById(android.R.id.content)

        book = Json.decodeFromString(intent.getStringExtra("bookData") ?:"")

        initBackButton()
        initBookInfo()
    }

    private fun initBackButton() {
        appBarLayout.setOnClickListener {
            // finish()
        }
    }

    private fun initBookInfo(){
        bookCardTitle.text = book.attributes.title
        bookCardCategory.text = book.attributes.content
        bookDescription.text = book.attributes.content

        getAuthorFromBook(applicationContext, parentView, book.relationships.authors.links.related){ author ->
            bookCardAuthor.text = author.attributes.name
            authorCardName.text = author.attributes.name
        }

    }
}