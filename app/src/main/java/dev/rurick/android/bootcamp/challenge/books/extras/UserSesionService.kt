package dev.rurick.android.bootcamp.challenge.books.extras

import android.content.Context
import android.util.Log
import android.view.View
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.snackbar.Snackbar
import dev.rurick.android.bootcamp.challenge.books.R
import org.json.JSONObject

private const val fileTag = "[USER_SESSION_SERVICE]"

fun getSessionToken(context: Context): String {
    val sharedPreferences = getSharedPreferences(context)
    return sharedPreferences.getString("token", "")!!
}

fun saveSessionToken(context: Context, jsonObject: JSONObject) {
    val sharedPreferences = getSharedPreferences(context)
    with(sharedPreferences.edit()) {
        putString("token", jsonObject.getString("plain-text-token"))
        apply()
    }
}

fun saveUserData(context: Context, userData: String) {
    val sharedPreferences = getSharedPreferences(context)
    with(sharedPreferences.edit()) {
        putString("user", userData)
        apply()
    }
}

fun getUserData(context: Context): String {
    val sharedPreferences = getSharedPreferences(context)
    return sharedPreferences.getString("user", "")!!
}

fun isValidSession(context: Context): Boolean {
    val sharedPreferences = getSharedPreferences(context)
    val token = sharedPreferences.getString("token", "")
    return !token.isNullOrEmpty()
}

fun logIn(context: Context, parentView: View, email: String, password: String){
    val logTag = "$fileTag - LOG_IN"

    val requestBody = JSONObject()
    requestBody.put("email", email)
    requestBody.put("password", password)
    requestBody.put("device_name", "user's phone")

    val loginRequest = JsonObjectRequest(
        Request.Method.POST,
        context.getString(R.string.base_url) + context.getString(R.string.endpoint_login),
        requestBody,
        { response ->
            val jsonObject = JSONObject(response.toString())
            saveSessionToken(context, jsonObject)
            if (isValidSession(context)) {
                fetchUserData(context, parentView)
            } else {
                throw Exception("invalid session")
            }
        },
        { error ->
            Log.e(
                logTag,
                "login failed with status code ${error?.networkResponse?.statusCode}"
            )
            Snackbar.make(parentView, "Wrong Credentials", Snackbar.LENGTH_SHORT).show()
        },
    )
    makeHTTPRequest(context, parentView, loginRequest)
}

fun logout(context: Context, parentView: View) {
    val logTag = "$fileTag - LOGOUT"

    getSharedPreferences(context).edit().clear().apply()

    val logoutRequest = object : JsonObjectRequest(
        Method.POST,
        context.getString(R.string.base_url) + context.getString(R.string.endpoint_logout),
        null,
        {
            navigateToLogIn(context)
        },
        { error ->
            Log.e(
                logTag,
                "get user failed with status code ${error?.networkResponse?.statusCode}"
            )
            Snackbar.make(parentView, "Failed to invalid token", Snackbar.LENGTH_SHORT).show()
        }
    ) {
        override fun getHeaders(): Map<String, String> {
            val params: MutableMap<String, String> = mutableMapOf()
            params["Authorization"] = "Bearer " + getSessionToken(context)
            params["Accept"] = "application/json"
            params["Content-Type"] = "application/json"
            return params
        }
    }

    makeHTTPRequest(context, parentView, logoutRequest)
}

fun fetchUserData(context: Context, parentView: View) {
    val logTag = "$fileTag - FETCH_USER_DATA"

    val userData = getUserData(context)
    if (userData.isNotEmpty()) {
        navigateToHome(context, userData)
    } else {
        val userRequest = object : JsonObjectRequest(
            Method.GET,
            context.getString(R.string.base_url) + context.getString(R.string.endpoint_user),
            null,
            { response ->
                val userDataJson = response.toString()
                saveUserData(context, userDataJson)
                navigateToHome(context, userDataJson)
            },
            { error ->
                Log.e(
                    logTag,
                    "get user failed with status code ${error?.networkResponse?.statusCode}"
                )
                Snackbar.make(parentView, "Wrong Credentials", Snackbar.LENGTH_SHORT).show()
            }
        ) {
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = mutableMapOf()
                params["Authorization"] = "Bearer " + getSessionToken(context)
                params["Accept"] = "application/json"
                params["Content-Type"] = "application/json"
                return params
            }
        }

        makeHTTPRequest(context, parentView, userRequest)
    }
}