package dev.rurick.android.bootcamp.challenge.books.extras

import android.content.Context
import android.util.Log
import android.view.View
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.material.snackbar.Snackbar
import dev.rurick.android.bootcamp.challenge.books.R
import dev.rurick.android.bootcamp.challenge.books.models.book.Book
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.json.JSONObject

private const val fileTag = "[BOOK]"

fun getAllBooks(
    context: Context,
    parentView: View,
    onResponse: (books: MutableList<Book>) -> Unit
) {
    val logTag = "$fileTag - GET_ALL_BOOKS"

    val allBooksRequest = object : JsonObjectRequest(
        Method.GET,
        context.getString(R.string.base_url) + context.getString(R.string.endpoint_books),
        null,
        { response ->
            val booksJsons = JSONObject(response.toString()).getJSONArray("data")
            val books: MutableList<Book> = mutableListOf()
            (0 until (booksJsons.length())).forEach {
                val json = booksJsons.getJSONObject(it).toString()
                val book = Json.decodeFromString<Book>(json)
                books.add(book)
            }
            onResponse(books)

        },
        { error ->
            Log.e(
                logTag,
                "fetch books failed with status code ${error?.networkResponse?.statusCode}"
            )
            Snackbar.make(parentView, "Wrong Credentials", Snackbar.LENGTH_SHORT).show()
        },
    ) {
        override fun getHeaders(): Map<String, String> {
            val params: MutableMap<String, String> = mutableMapOf()
            params["Authorization"] = "Bearer " + getSessionToken(context)
            params["Accept"] = "application/json"
            params["Content-Type"] = "application/json"
            return params
        }
    }
    makeHTTPRequest(context, parentView, allBooksRequest)
}