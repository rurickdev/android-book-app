package dev.rurick.android.bootcamp.challenge.books.extras

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import dev.rurick.android.bootcamp.challenge.books.R


fun getSharedPreferences(context: Context): SharedPreferences {
    return EncryptedSharedPreferences.create(
        context.getString(R.string.shared_preferences_name),
        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
        context,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
}