package dev.rurick.android.bootcamp.challenge.books

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import dev.rurick.android.bootcamp.challenge.books.extras.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private lateinit var parentView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        parentView = findViewById(android.R.id.content)
        if (isValidSession(applicationContext)) {
            fetchUserData(applicationContext, parentView)
            // finish()
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
    }

    private fun init() {
        initEmailInput()
        initPasswordInput()
        initLoginButton()
        initSignInButton()
    }

    private fun initEmailInput() {
        // TODO("Validate email")
    }

    private fun initPasswordInput() {
        // TODO("Validate Password")
    }

    private fun initSignInButton() {
        signInTextButton.setOnClickListener {
            navigateToSignIn(applicationContext)
        }
    }

    private fun initLoginButton() {
        loginRaisedButton.setOnClickListener {
            // TODO: validate email and password
            val email = emailTextInput.text?.toString() ?: ""
            val password = passwordTextInput.text?.toString() ?: ""
            logIn(applicationContext, parentView, email, password)
            // finish()
        }
    }
}