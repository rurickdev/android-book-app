package dev.rurick.android.bootcamp.challenge.books.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import dev.rurick.android.bootcamp.challenge.books.BookDetailsActivity
import dev.rurick.android.bootcamp.challenge.books.R
import dev.rurick.android.bootcamp.challenge.books.models.book.Book
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


class BookCardAdapter(private var mContext:Context, private val cardsData: MutableList<Book>) :
    RecyclerView.Adapter<BookCardAdapter.BookCardHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookCardHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return BookCardHolder(mContext, layoutInflater.inflate(R.layout.book_card_view, parent, false))
    }

    override fun onBindViewHolder(holder: BookCardHolder, position: Int) {
        holder.render(bookCardData = cardsData[position])
    }

    override fun getItemCount(): Int = cardsData.size

    class BookCardHolder(private var mContext: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val bookCardView: MaterialCardView = itemView.findViewById(R.id.bookCardView)
        private val textViewTitle: TextView = itemView.findViewById(R.id.bookCardTitle)
        private val textViewAuthor: TextView = itemView.findViewById(R.id.bookCardAuthor)
        private val textViewCategory: TextView = itemView.findViewById(R.id.bookCardCategory)

        fun render(bookCardData: Book) {
            textViewTitle.text = bookCardData.attributes.title
            textViewAuthor.text = bookCardData.relationships.authors.links.self
            textViewCategory.text = bookCardData.attributes.content

            bookCardView.setOnClickListener {
                val bookDetailsIntent = Intent(mContext, BookDetailsActivity::class.java)
                bookDetailsIntent.putExtra("bookData", Json.encodeToString(bookCardData))
                mContext.startActivity(bookDetailsIntent)
            }
        }
    }
}