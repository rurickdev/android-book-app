package dev.rurick.android.bootcamp.challenge.books.models.author

import dev.rurick.android.bootcamp.challenge.books.models.Links
import kotlinx.serialization.Serializable

@Serializable
data class Author (
    val type: String,
    val id: String,
    val attributes: AuthorAttributes,
    val relationships: AuthorRelationships,
    val links: Links
)