package dev.rurick.android.bootcamp.challenge.books.extras

import android.content.Context
import android.content.Intent
import dev.rurick.android.bootcamp.challenge.books.HomeActivity
import dev.rurick.android.bootcamp.challenge.books.LoginActivity
import dev.rurick.android.bootcamp.challenge.books.SignInActivity

fun navigateToHome(context: Context, userDataJson: String){
    val homeIntent = Intent(context, HomeActivity::class.java)
    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    homeIntent.putExtra("userData", userDataJson)
    context.startActivity(homeIntent)
}
fun navigateToLogIn(context: Context){
    val logInIntent = Intent(context, LoginActivity::class.java)
    logInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    logInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context.startActivity(logInIntent)
}
fun navigateToSignIn(context: Context){
    val logInIntent = Intent(context, SignInActivity::class.java)
    logInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    context.startActivity(logInIntent)
}