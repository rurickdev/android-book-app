package dev.rurick.android.bootcamp.challenge.books

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dev.rurick.android.bootcamp.challenge.books.adapters.BookCardAdapter
import dev.rurick.android.bootcamp.challenge.books.extras.getAllBooks
import dev.rurick.android.bootcamp.challenge.books.models.UserData
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.util.*

class HomeActivity : AppCompatActivity() {
    private val logTag = "HOME_ACTIVITY"
    private lateinit var cardAdapter: BookCardAdapter
    private lateinit var parent: View
    private lateinit var userData: UserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        parent = findViewById(android.R.id.content)

        val userIntentData: String = intent.getStringExtra("userData") ?: ""
        userData = Json.decodeFromString(userIntentData)

        initCardRecycler()
        initHeader()
    }

    private fun initHeader() {
        appBarUserName.text = userData.name.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(
                Locale.getDefault()
            ) else it.toString()
        }
    }

    private fun initCardRecycler() {
        getAllBooks(applicationContext, parent) { books ->
            bookCardRecyclerView.layoutManager = LinearLayoutManager(this)
            bookCardRecyclerView.setHasFixedSize(true)
            cardAdapter = BookCardAdapter(this, books)
            bookCardRecyclerView.adapter = cardAdapter
        }
    }
}