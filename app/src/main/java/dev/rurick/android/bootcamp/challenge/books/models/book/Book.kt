package dev.rurick.android.bootcamp.challenge.books.models.book

import dev.rurick.android.bootcamp.challenge.books.models.Links
import kotlinx.serialization.Serializable

@Serializable
data class Book(
    val attributes: BookAttributes,
    val id: String,
    val links: Links,
    val relationships: BookRelationships,
    val type: String
)
