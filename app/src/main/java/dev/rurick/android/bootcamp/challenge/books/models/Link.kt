package dev.rurick.android.bootcamp.challenge.books.models

import kotlinx.serialization.Serializable

@Serializable
data class LinkContainer(
    val links: Links
)

@Serializable
data class Links(
    val related: String = "",
    val self: String
)