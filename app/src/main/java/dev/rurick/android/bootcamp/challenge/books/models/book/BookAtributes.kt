package dev.rurick.android.bootcamp.challenge.books.models.book

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BookAttributes(
    val content: String,
    @SerialName("created-at")
    val createdAt: String,
    val slug: String,
    val title: String,
    @SerialName("updated-at")
    val updatedAt: String
)