package dev.rurick.android.bootcamp.challenge.books

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import dev.rurick.android.bootcamp.challenge.books.extras.fetchUserData
import dev.rurick.android.bootcamp.challenge.books.extras.isValidSession
import dev.rurick.android.bootcamp.challenge.books.extras.saveSessionToken
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.json.JSONObject

class SignInActivity : AppCompatActivity() {
    private val logTag = "SIGN_IN_ACTIVITY"
    private lateinit var parentView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        parentView = findViewById(android.R.id.content)
        if (isValidSession(applicationContext)) {
            fetchUserData(applicationContext, parentView)
            // finish()
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        init()
    }

    private fun init() {
        initBackButton()
        initSignInButton()
    }

    private fun initSignInButton() {
        signInButton.setOnClickListener {
            val userName = userTextInput.text
            val email = emailTextInput.text
            val password = passwordTextInput.text
            val confirmPassword = confirmPasswordTextInput.text

            // Instantiate the RequestQueue.
            val queue = Volley.newRequestQueue(applicationContext)
            val url = getString(R.string.base_url) + getString(R.string.endpoint_register)
            val requestBody = JSONObject()
            requestBody.put("name", userName)
            requestBody.put("email", email)
            requestBody.put("password", confirmPassword)
            requestBody.put("password_confirmation", password)
            requestBody.put("device_name", "user's phone")

            // Request a string response from the provided URL.
            val loginRequest = JsonObjectRequest(
                Request.Method.POST,
                url,
                requestBody,
                { response ->
                    val jsonObject = JSONObject(response.toString())
                    saveSessionToken(applicationContext, jsonObject)
                    if (isValidSession(applicationContext)) {
                        fetchUserData(applicationContext, parentView)
                        // finish()
                    } else {
                        throw Exception("invalid session")
                    }
                },
                { error ->
                    Log.e(
                        logTag,
                        "login failed with status code ${error?.networkResponse?.statusCode}"
                    )
                    Snackbar.make(parentView, "Wrong Credentials", Snackbar.LENGTH_SHORT).show()
                },
            )

            // Add the request to the RequestQueue.
            queue.add(loginRequest)
        }
    }

    private fun initBackButton() {
        appBarLayout.setOnClickListener {
            // finish()
        }
    }
}